import React from 'react'
import Dashboard from './components/Dashboard';
import GuestRoute from './components/GuestRoute'
import AuthRoute from './components/AuthRoute'
import Login from './components/Login'
import Register from './components/Register'
import {BrowserRouter as Router, Route, Link, Switch} from 'react-router-dom'
import NotFound from './components/NotFound';
import Header from './components/Header'
import Footer from './components/Footer'
import SingleProject from './components/SingleProject'
import ModelsDashboard from './components/ModelsDashboard'
import Predict from './components/Predict'
import ModelTree from './components/ModelTree'

class AppRouter extends React.Component {
    render() {
        return (
            <Router>
                <Header></Header>
                <div className="container main">
                    <Switch>
                        <AuthRoute path="/" component={Dashboard} exact={true}></AuthRoute>
                        <GuestRoute path="/login" component={Login}></GuestRoute>
                        <GuestRoute path="/index.html" component={Login}></GuestRoute>
                        <GuestRoute path="/signup" component={Register}></GuestRoute>
                        <AuthRoute path="/dashboard" component={Dashboard}></AuthRoute>
                        <AuthRoute path="/projects/:id" component={SingleProject}></AuthRoute>
                        <AuthRoute path="/projects" exact={true} component={Dashboard}></AuthRoute>
                        <AuthRoute path="/models/:id/predict" component={Predict}></AuthRoute>
                        <AuthRoute path="/models/:id/tree" component={ModelTree}></AuthRoute>
                        <AuthRoute path="/models" component={ModelsDashboard}></AuthRoute>
                        <Route path="*" component={NotFound}></Route>
                    </Switch>
                </div>
                <Footer></Footer>
            </Router>
        )
    }
}

export default AppRouter
