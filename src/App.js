import React from 'react';
import './App.css';
import 'bootstrap'
import AppRouter from './AppRouter'


function App() {
  return (
    <div className="container-fluid">
     <AppRouter></AppRouter>
    </div>
  );
}

export default App;
