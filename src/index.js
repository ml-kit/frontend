import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import store from './store/index'
import {Provider} from 'react-redux'
import $ from 'jquery'
import cookie from 'js-cookie'
import jwt from 'jsonwebtoken'

const API_URL = process.env.REACT_APP_API_URL
const TOKEN_SECRET = process.env.REACT_APP_TOKEN_SECRET

let token = sessionStorage.getItem('token')
const username = sessionStorage.getItem('username')

const destroyCookie = () => {
  token = null
  sessionStorage.removeItem('token')
  sessionStorage.removeItem('username')
}

if (token) {
  jwt.verify(token, `${TOKEN_SECRET}`, (err, decoded) => {
    if (err) {
      destroyCookie()
    } else if (decoded.aud !== 'fastapi-users:auth') {
      destroyCookie()
    }
  })
}

const render = () => {
    ReactDOM.render(

        <React.StrictMode>
        <Provider store={store}>
            <App/>
        </Provider>
    </React.StrictMode>, document.getElementById('root'));
}

if (token) {
    const logginUrl = `${API_URL}/users/me`
    const logginSettings = {
        "url": logginUrl,
        "method": "GET",
        "headers": {
            "Content-Type": "application/json"
        },
        beforeSend: (xhr) => {
            xhr.setRequestHeader("Authorization", "Bearer " + token);
        },
        "data": {
            "username": username
        }
    };

    $
        .ajax(logginSettings)
        .done((response) => {
            store.dispatch({
                type: "SET_LOGIN",
                payload: {
                    username: response.email
                }
            })
        })
        .fail((error) => {
        })

      render()

} else {
  render()
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls. Learn
// more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
