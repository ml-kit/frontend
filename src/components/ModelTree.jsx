import React from 'react'
import {Link} from 'react-router-dom'
import axios from 'axios'
import { MapInteractionCSS } from 'react-map-interaction'


const API_URL = process.env.REACT_APP_API_URL
const token = sessionStorage.getItem('token')

class ModelTree extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
          id: props.match.params.id,
          depth: 1,
          tree: null,
          model: null,
          onRender: false
        }
        this.getTree = this.getTree.bind(this)
        this.setDepth = this.setDepth.bind(this)
    }

    componentDidMount() {
      this.loadModel()
    }

    async loadModel() {
      this.setState((prev) => {
        return {
            ...prev,
            onRender: true
        }
      })

      const res = await axios.get(`${API_URL}/models/${this.state.id}`, {
        headers: {
          'Content-Type': 'multipart/form-data',
          "Authorization": "Bearer " + token
        }
      })

    this.setState((prev) => {
        return {
            ...prev,
            model: res.data,
            onRender: false
        }
    })
    }

    async getTree(e) {
      e.preventDefault()

        try {
            const res = await axios.get(`${API_URL}/models/${this.state.id}/interpretation/decision_tree/?tree_id=${this.state.depth - 1}`, {
                headers: {
                  'Content-Type': 'multipart/form-data',
                  "Authorization": "Bearer " + token
                }
              })
    
            this.setState(prevState => {
                return {
                    ...prevState,
                    tree: res.data
                }
            })
        } catch (err) {

        }

    }

    setDepth(e) {
      const value = e.target.value

      this.setState(prevState => {
        return {
            ...prevState,
            depth: value
        }
      })
    }

    render() {
        return (
            <div className="row">
                <div className="col-12">
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item">
                                <Link to="/">Home</Link>
                            </li>
                            <li className="breadcrumb-item">
                              <Link to="/models">Models</Link>
                            </li>
                            <li className="breadcrumb-item active" aria-current="page">
                              {this.state.id}
                            </li>
                            <li className="breadcrumb-item active" aria-current="page">
                                Tree
                            </li>
                        </ol>
                    </nav>
                </div>
                <div className="col-md-6 offset-md-3">
                    <h1 className="text-center mb-4">Model tree</h1>
                    <form onSubmit={this.getTree}>
                      <div className="input-group mb-3">
                        {this.state.model && <input type="number" min="1" max={this.state.model.estimators} required onChange={this.setDepth} className="form-control" placeholder={`Number of tree in ansamble (max - ${this.state.model.estimators})`} /> }
                        <div className="input-group-append">
                          <button className="btn btn-outline-primary" disabled={this.state.onRender} type="submit">Render</button>
                        </div>
                    </div>
                    </form>
                </div>
                {this.state.tree && <div className="col-12 model-tree mb-4">     
                    <MapInteractionCSS defaultScale={0.5} maxScale={3} showControls={true} btnClass="btn btn-primary m-2"><img src={`data:image/jpeg;base64,${this.state.tree}`}/></MapInteractionCSS>
                </div>}
            </div>
                )
    }
}

export default ModelTree