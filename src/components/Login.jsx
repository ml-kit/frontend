import React from 'react'
import {Form, Button, FormControl, FormLabel, FormGroup} from 'react-bootstrap'
import $ from 'jquery'
import {Link} from 'react-router-dom'
import {connect} from 'react-redux'

const API_URL = process.env.REACT_APP_API_URL

class Login extends React.Component {

    constructor(props) {
        super(props)
        this.HandleForm = this
            .HandleForm
            .bind(this)
        this.HandleInput = this
            .HandleInput
            .bind(this)
        this.state = {
            username: '',
            password: '',
            error: ''
        }
    }

    HandleForm = (e) => {
        e.preventDefault()

        const data = {
            username: this.state.username,
            password: this.state.password
        }
        const url = `${API_URL}/users/login/jwt`

        const settings = {
            "url": url,
            "method": "POST",
            "headers": {
                "Content-Type": "application/x-www-form-urlencoded",
                "Access-Control-Allow-Origin": "*"
            },
            "data": {
                "username": data.username,
                "password": data.password
            }
        };

        $
            .ajax(settings)
            .done((response) => {
                const token = response.token

                sessionStorage.setItem("token", token)
                sessionStorage.setItem("username", data.username)

                this
                    .props
                    .setLogin({username: data.username})

                setTimeout(() => {
                    this
                    .props
                    .history
                    .push('/dashboard')
                }, 0)
                    
            })
            .fail((error) => {
                if (error.status == 400) {
                    this.setState({error: error.responseJSON.detail})
                }
            })

    }

    HandleInput = (e) => {
        e.preventDefault()

        const name = e.target.name
        const value = e.target.value

        this.setState({[name]: value})
    }

    render() {

        return (
            <div className="row">
                <div className="col-md-6 offset-md-3 offset-0">
                    <h1 className="text-center">Log in</h1>
                    <Form onSubmit={this.HandleForm}>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email</Form.Label>
                            <Form.Control
                                type="email"
                                name="username"
                                required
                                onChange={this.HandleInput}
                                placeholder="Enter email"/>
                            <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control
                                type="password"
                                name="password"
                                required
                                onChange={this.HandleInput}
                                placeholder="Password"/>
                        </Form.Group>
                        <p className="error-field">
                            <Button className="mr-2" variant="primary" type="submit">
                                Submit
                            </Button>
                            {this.state.error}
                        </p>
                        <p className="login-register-link"> or
                            <Link to="/signup" className="ml-2">sign up</Link>
                        </p>
                    </Form>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setLogin: user => dispatch({type: "SET_LOGIN", payload: user})
    }
}

export default connect(null, mapDispatchToProps)(Login)