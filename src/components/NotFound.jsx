import React from 'react'
import {Row} from 'react-bootstrap'

class NotFound extends React.Component {
    render() {
        return (
            <Row>
                <div className="col-md-4 offset-md-4 offset-0">
                    <h1 className="text-center">Page not found</h1>
                </div>
            </Row>
        )
    }
}

export default NotFound