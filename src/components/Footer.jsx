import React, {Fragment} from 'react'
import {Row} from 'react-bootstrap'

function Footer() {

    return (
        <Row className="footer">
            <div className="container">
                <div className="col-12 mt-3 mb-2">
                    <p className="text-center">ML-kit | copyright © 2020</p>
                </div>
            </div>
        </Row>
    )
}

export default Footer