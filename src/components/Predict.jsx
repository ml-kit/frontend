import React from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'
import { CSVLink, CSVDownload } from 'react-csv'

const API_URL = process.env.REACT_APP_API_URL
const token = sessionStorage.getItem('token')

class Predict extends React.Component {
    constructor(props) {
      super(props)

      let id = props.match.params.id
      this.state = {
        id,
        model: null,
        project: null,
        formAttr: {},
        csvFile: null,
        results: [],
        result: '',
        columns: [],
        csvIndex: '',
        error: null
      }

      this.handleFormAttrs = this.handleFormAttrs.bind(this)
      this.formPredict = this.formPredict.bind(this)
      this.handleCsvFile = this.handleCsvFile.bind(this)
      this.filePredict = this.filePredict.bind(this)
      this.handleCsvIndex = this.handleCsvIndex.bind(this)
    }

    componentDidMount() {
      this.loadModel()
    }

    async loadModel() {
      const res = await axios.get(`${API_URL}/models/${this.state.id}`, {
        headers: {
          'Content-Type': 'multipart/form-data',
          "Authorization": "Bearer " + token
        }
      })

      const project = await axios.get(`${API_URL}/projects/${res.data.project}`, {
        headers: {
          'Content-Type': 'multipart/form-data',
          "Authorization": "Bearer " + token
        }
      })

      const columns = project.data.columns

    this.setState((prev) => {
        return {
            ...prev,
            columns,
            csvIndex: columns[0].name,
            model: res.data
        }
    })
    }

    handleFormAttrs(e) {
      const { name, value } = e.target

      const formAttr = this.state.formAttr

      if (value === 'Select') {
        delete formAttr[name]
      } else {
        formAttr[name] = value;
      }
      

      this.setState((prev) => {
        return {
          ...prev,
          formAttr
        }
      })
    }

    handleCsvFile(e) {
      const csvFile = e.target.files[0]

      this.setState(prev => {
        return {
          ...prev,
          csvFile
        }
      })
    }

    handleCsvIndex(e) {
      const csvIndex = e.target.value

      this.setState(prev => {
        return {
          ...prev,
          csvIndex
        }
      })
    }

    async filePredict(e) {
      e.preventDefault()

      const formData = new FormData();
      formData.append('file', this.state.csvFile);
      formData.append('indexColName', this.state.csvIndex);

      try {
        const res = await axios.post(`${API_URL}/models/${this.state.id}/predict/csv/`,formData, {
          headers: {
            'Content-Type': 'multipart/form-data',
            "Authorization": "Bearer " + token
          }
        })

        const results = res.request.response.split('\n').map(t => t.split(','))

        results.pop()

        this.setState(prev => {
          return {
            ...prev,
            result: '',
            error: null,
            results
          }
        })
      } catch(e) {
        this.setState(prev => {
          return {
            ...prev,
            error: 'Something went wrong'
          }
        })
      }
    }

    async formPredict(e) {
      e.preventDefault()

      try {
        const res = await axios.post(`${API_URL}/models/${this.state.id}/predict`,this.state.formAttr, {
          headers: {
            'Content-Type': 'multipart/form-data',
            "Authorization": "Bearer " + token
          }
        })
        this.setState(prev => {
          return {
            ...prev,
            result: res.request.response,
            results: [],
            error: null
          }
        })

      } catch(e) {
        this.setState(prev => {
          return {
            ...prev,
            error: 'Something went wrong'
          }
        })
      }
      
    }

    render() {
        return (
            <div className="row">
            <div className="col-12">
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item">
                                <Link to="/">Home</Link>
                            </li>
                            <li className="breadcrumb-item">
                              <Link to="/models">Models</Link>
                            </li>
                            <li className="breadcrumb-item active" aria-current="page">
                              {this.state.id}
                            </li>
                            <li className="breadcrumb-item active" aria-current="page">
                                Predict
                            </li>
                        </ol>
                    </nav>
                </div>
                <div className="col-md-12">
                    <h1 className="text-center underline mb-4">Predict</h1>
                </div>
                <div className="col-md-4">
                  <h2 className="text-center">with form</h2>
                  <form onSubmit={this.formPredict}>
                    <div className="form-group row">
                      {this.state.model && this.state.model.requiredColumns.map(column => {

                          const columnAttr = this.state.columns.filter(t => t.name === column)[0]

                          if (columnAttr.categories === true) {
                            return (
                              <div key={column} className="col-md-6">
                                <label htmlFor="param">{column} ({columnAttr.dataType})</label>
                                <select onChange={this.handleFormAttrs} className="form-control" required name={column} type="text">
                                  <option  value=''>Select</option>
                                  {columnAttr.uniqueValues.map(val => {
                                    return <option key={val} value={val}>{val}</option>
                                  })}
                                </select>
                              </div>
                          )
                          } else {
                            return (
                              <div key={column} className="col-md-6">
                                <label htmlFor="param">{column} ({columnAttr.dataType})</label>
                                <input onChange={this.handleFormAttrs} className="form-control" required name={column} type="text"/>
                              </div>
                          )
                          }
                        })}
                        </div>
                        <div className="form-group">
                        <button type="submit" className="btn btn-primary">predict</button>
                      </div>
                    
                  </form>
                </div>
                <div className="col-md-4">
                  <h2 className="text-center">with csv</h2>
                  <form onSubmit={this.filePredict}>
                    <div className="form-group">
                      <label>Title of index column</label>
                      <select name="indexColName" onChange={this.handleCsvIndex} defaultValue={this.state.columns.length > 0 && this.state.columns[0].name} className="form-control">
                        {this.state.columns.map(t => <option key={t.name}>{t.name}</option>)}
                      </select>
                    </div>
                    <div className="form-group">
                        
                      <div className="input-group mb-3 csv-predict">
                        <div className="input-group-prepend">
                          <span className="input-group-text" id="inputGroupFileAddon01">Upload .csv</span>
                        </div>
                        <div className="custom-file">
                          <input onChange={this.handleCsvFile} type="file" required className="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01" />
                          <label className="custom-file-label" >Choose file</label>
                        </div>
                      </div>
                    </div>
                    <button className="btn btn-primary">predict</button>
                  </form>
                </div>
                <div className="col-md-4 query-example">
                <h2>API’s query example</h2>
                <p className="api-example">const settings =  {'{'}<br></br>
                    
                    &emsp;"url": "<a href={`${API_URL}/models/${this.state.id}/predict/`}>{`${API_URL}/models/${this.state.id}/predict/`}</a>",<br></br>
                    &emsp;"method": “POST,<br></br>
                    &emsp;// <b>.../predict/csv</b> for csv predict<br></br>
                    &emsp;“data”: {'{'}<br></br>
                    &emsp;&emsp;key1: val1,<br></br>
                    &emsp;&emsp;key2: val2,<br></br>
                    &emsp;&emsp;key3: val3,<br></br>
                    &emsp;&emsp;...<br></br>
                    &emsp; {'}'}<br></br>
                    {'}'};<br></br>
                    $.ajax(settings).done(response -> console.log(response))</p>
                  <div className="row">
                
                  </div>
            </div>
              <div className="col-md-4 offset-md-4 text-center">
                {this.state.results.length > 0 && <h2 className="text-center">Results:</h2>}
                {this.state.results.length > 0 && <CSVLink data={this.state.results} className="btn btn-success mt-2 mb-3" filename={`${this.state.model.projectName}.csv`}>Download csv</CSVLink>}
                  {this.state.results.length> 0 && this.state.results.map((t,index) => {
                    const className = index % 2 !== 0 ? 'predict-result mark' : 'predict-result'
                    return <p key={index} className={className}><span>{t[0]}</span><span>{t[1]}</span></p>
                  })}
                {this.state.result.length> 0 && <h2 className="text-center">Result: {this.state.result}</h2>}

                {
                  this.state.error && <div className="alert alert-danger text-center">
                      {this.state.error}
                    </div>
                }
              </div>
            </div>
        )
    }
}

export default Predict