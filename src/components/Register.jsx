import React from 'react'
import {Form, Button, FormControl, FormLabel, FormGroup} from 'react-bootstrap'
import $ from 'jquery'
import {Link} from 'react-router-dom'
import cookie from 'js-cookie'
import {connect} from 'react-redux'

const API_URL = process.env.REACT_APP_API_URL

class Register extends React.Component {
    constructor(props) {
        super(props)
        this.HandleForm = this
            .HandleForm
            .bind(this)
        this.HandleInput = this
            .HandleInput
            .bind(this)
        this.state = {
            email: '',
            password: '',
            confirm_password: '',
            error: ''
        }
    }

    HandleForm = (e) => {
        e.preventDefault()

        const data = {
            email: this.state.email,
            password: this.state.password,
            confirm_password: this.state.confirm_password
        }

        if (data.password !== data.confirm_password) {
            this.setState({error: 'Passwords dont match'})
            return
        }
        const url = `${API_URL}/users/register`

        const settings = {
            "url": url,
            "method": "POST",
            "headers": {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*"
            },
            "data": JSON.stringify(data)
        };

        $
            .ajax(settings)
            .done((response) => {
                const logginUrl = `${API_URL}/users/login/jwt`

                const logginSettings = {
                    "url": logginUrl,
                    "method": "POST",
                    "headers": {
                        "Content-Type": "application/x-www-form-urlencoded",
                        "Access-Control-Allow-Origin": "*"
                    },
                    "data": {
                        "username": data.email,
                        "password": data.password
                    }
                };

                $
                    .ajax(logginSettings)
                    .done((response) => {
                        const token = response.token

                        sessionStorage.setItem("token", token)
                        sessionStorage.setItem("username", data.email)

                        this
                            .props
                            .setLogin({username: data.email})

                        this
                            .props
                            .history
                            .push('/dashboard')
                    })
                    
            })
            .fail((error) => {
                if (error.status == 400) {
                    this.setState({error: error.responseJSON.detail})
                }
            })

    }

    HandleInput = (e) => {
        e.preventDefault()

        const name = e.target.name
        const value = e.target.value

        this.setState({[name]: value})
    }

    render() {
        return (
            <div className="row">
                <div className="col-md-6 offset-md-3 offset-0">
                    <h1 className="text-center">Sign up</h1>
                    <Form onSubmit={this.HandleForm}>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email</Form.Label>
                            <Form.Control
                                type="email"
                                name="email"
                                required
                                onChange={this.HandleInput}
                                placeholder="Enter email"/>
                            <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control
                                type="password"
                                name="password"
                                required
                                onChange={this.HandleInput}
                                placeholder="Password"/>
                        </Form.Group>
                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Repeat password</Form.Label>
                            <Form.Control
                                type="password"
                                name="confirm_password"
                                required
                                onChange={this.HandleInput}
                                placeholder="Repeat password"/>
                        </Form.Group>
                        <p className="error-field">
                            <Button className="mr-2" variant="primary" type="submit">
                                Submit
                            </Button>
                            {this.state.error}
                        </p>
                        <p className="login-register-link">or
                            <Link to="/login" className="ml-2">sign in</Link>
                        </p>
                    </Form>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setLogin: user => dispatch({type: "SET_LOGIN", payload: user})
    }
}

export default connect(null, mapDispatchToProps)(Register)