import React from 'react'
import {Route, Redirect} from 'react-router-dom'
import cookie from 'js-cookie'
import {connect} from 'react-redux'

const AuthRoute = ({
    component: Component,
    ...props
}) => {
    const token = sessionStorage.getItem('token')

    return (
        <Route
            {...props}
            render={(props) => token
            ? (<Component {...props}/>)
            : (<Redirect
                to={{
                pathname: "/login",
                state: {
                    from: props.location
                }
            }}/>)}/>
    );
}

const mapStateToProps = state => {
    return {loggedIn: state.auth.loggedIn, user: state.auth.user}
}

export default connect(mapStateToProps)(AuthRoute)