import React, {Fragment} from 'react'
import {Link} from 'react-router-dom'
import {Row} from 'react-bootstrap'
import {connect} from 'react-redux'
import cookie from 'js-cookie'
import $ from 'jquery'

const API_URL = process.env.REACT_APP_API_URL

function Header(props) {

    const handleLogout = (e) => {
        e.preventDefault()

        const username = sessionStorage.getItem('username')
        const token = sessionStorage.getItem('token')

        const logoutnUrl = `${API_URL}/users/logout/jwt`

        const logoutSettings = {
            "url": logoutnUrl,
            "method": "POST",
            "headers": {
                "Content-Type": "application/json"
            },
            beforeSend: (xhr) => {
                xhr.setRequestHeader("Authorization", "Bearer " + token);
            },
            "data": {
                "username": username
            }
        };

        $
            .ajax(logoutSettings)
            .done((response) => {
                sessionStorage.removeItem('token')
                sessionStorage.removeItem('username')
                props.setLogout()
            })
            

    }

    return (
        <div className="container">
            <Row className="header mb-3">
                <div className="col-md-7">
                    <h1 className="logo text-center text-md-left">
                        <Link to='/'>ML-Kit</Link>
                    </h1>
                </div>
                <div className="col-md-5 d-flex justify-content-md-end justify-content-center">
                    {props.loggedIn
                        ? (
                            <Fragment>
                                <Link className="logout-link text-right btn btn-success" to="/models">models API</Link>
                                <Link
                                    onClick={handleLogout}
                                    className="logout-link text-right btn btn-danger"
                                    to="/logout">Log out</Link>
                            </Fragment>
                        )
                        : (
                            <Link className="logout-link text-right btn btn-primary" to="/login">Log in</Link>
                        )
}
                </div>
            </Row>
        </div>
    )
}

const mapStateToProps = state => {
    return {loggedIn: state.auth.loggedIn, user: state.auth.user}
}

const mapDispatchToProps = dispatch => {
    return {
        setLogout: () => dispatch({type: 'SET_LOGOUT'})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header)