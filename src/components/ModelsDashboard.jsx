import React from 'react'
import {Link} from 'react-router-dom'
import axios from 'axios'
import { MapInteractionCSS } from 'react-map-interaction'


const API_URL = process.env.REACT_APP_API_URL
const token = sessionStorage.getItem('token')

class ModelsDashboard extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            modal: {
                isVisible: false,
                modelNumber: 0,
                data: {},
                feature_coefficients: {},
                error: '',
                log: '',
                charts: {}
            },
            models: [],
        }
        // <img src={`data:image/jpeg;base64,${binary_data}`} />
        this.showModal = this.showModal.bind(this)
        this.hideModal = this.hideModal.bind(this)
        this.showError = this.showError.bind(this)
        this.removeModel = this.removeModel.bind(this)
    }

    componentDidMount() {
        this.loadModels()
    }

    async loadModels() {
        const res = await axios.get(`${API_URL}/models/`, {
            headers: {
              'Content-Type': 'multipart/form-data',
              "Authorization": "Bearer " + token
            }
          })

        this.setState((prev) => {
            return {
                ...prev,
                models: res.data
            }
        })
    }

    showModal(e) {
        const index = e.target.dataset.index;
        const type = e.target.dataset.type;

        const {modal} = {
            ...this.state
        }

        const currentState = modal
        currentState.isVisible = true

        if (type === 'log') {
            currentState.data['log'] = this.state.models[index]['learningLogs'].split('\n').map(t => t.trim())
        } else {
            currentState.data['metrics'] = this.state.models[index]['metrics']
            currentState.data['feature_coefficients'] = this.state.models[index]['feature_coefficients']
            currentState['charts']['learning'] = this.state.models[index]['charts']['Learning']
        }

        this.setState(prevState => {
            return {
                ...prevState,
                modal: currentState
            }
        })
    }


    showError(e) {
        const error = e.target.value

        const {modal} = {
            ...this.state
        }

        const currentState = modal
        currentState.isVisible = true
        currentState.error = error

        this.setState(prevState => {
            return {
                ...prevState,
                modal: currentState
            }
        })
    }

    async removeModel(e) {
        const {models} = {
            ...this.state
        }

        const index = e.target.dataset.index;
        
        const currentState = models

        const modelId = currentState[index]._id

        currentState.splice(index, 1);

        try {
            const res = await axios.delete(`${API_URL}/models/${modelId}`, {
                headers: {
                  'Content-Type': 'multipart/form-data',
                  "Authorization": "Bearer " + token
                }
              })
    
            this.setState(prevState => {
                return {
                    ...prevState,
                    models: currentState
                }
            })
        } catch(e) {
            
        }

    }

    hideModal() {
        const {modal} = {
            ...this.state
        }

        const currentState = modal
        currentState.data = {}
        currentState.charts = {}
        currentState.error = ''
        currentState.isVisible = false

        this.setState(prevState => {
            return {
                ...prevState,
                modal: currentState
            }
        })
    }

    render() {
        return (
            <div className="row">
                <div className="col-12">
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item">
                                <Link to="/">Home</Link>
                            </li>
                            <li className="breadcrumb-item active" aria-current="page">
                                Models
                            </li>
                        </ol>
                    </nav>
                </div>
                <div className="col-12">
                    <h1 className="text-center mb-4">Your models</h1>
                    { this.state.modal.isVisible && <div className="metrics-modal">
                        <div className="row">
                            <div className="col-md-8 offset-md-2">
                                <div className="row ">
                                <div className="col-12 modal-scroll">
                                    {this.state.modal.data.log && <h3 className="text-center w-100">Learning log</h3>}
                                    {this.state.modal.data.log && this.state.modal.data.log.map(t => t != '' ? <p key={t} className="pb-0">{t}</p> : '')}

                                    {this.state.modal.error.length > 0 && <p className="not-saved mt-4">{this.state.modal.error}</p>}

                                    {this.state.modal.data.metrics && <h3 className="text-center w-100">Metrics</h3>}
                                    {this.state.modal.data.metrics && Object.keys(this.state.modal.data.metrics).map((key) => {
                                        return <p className="single-metric" key={key}>{key} - {this.state.modal.data.metrics[key]}</p>
                                    })}

                                    {this.state.modal.data.feature_coefficients && <h3 className="text-center w-100 d-inline-block">Feature Coefficients</h3>}
                                    {this.state.modal.data.feature_coefficients && Object.keys(this.state.modal.data.feature_coefficients).map(key => <p key={key} className="pb-0 w-50">{key} - {this.state.modal.data.feature_coefficients[key]}</p>)}

                                    {this.state.modal.charts.learning && <h3 className="text-center w-100 d-inline-block">Learning chart</h3>}
                                    {this.state.modal.charts.learning && <img src={this.state.modal.charts.learning} alt=""/>}


                                    <button onClick={this.hideModal} className="btn btn-danger">Close</button>
                                </div>
                                   
                                </div>
                            </div>
                        </div> 
                    </div>
                    }
                </div>
                <div className="col-md-12">
                    <div className="row">
                        <div className="col-md-2">
                            <h5>Project name</h5>
                        </div>
                        <div className="col-md-2">
                            <h5>Learning method</h5>
                        </div>
                        <div className="col-md-2">
                            <h5>Model type</h5>
                        </div>
                        <div className="col-md-1">
                            <h5>Status</h5>
                        </div>
                        <div className="col-md-1">
                            <h5>Tree</h5>
                        </div>
                        <div className="col-md-1">
                            <h5>Metrics</h5>
                        </div>  
                        <div className="col-md-1">
                            <h5>Log</h5>
                        </div>   
                        <div className="col-md-1">
                            <h5>Predict</h5>
                        </div>                
                    </div>
                    {this.state.models.map((model, index) => {
                        return (
                            <div className="row single-model mt-2 mb-4" key={model._id}>
                                <div className="col-md-2 d-flex align-items-center">
                                    <p><Link className="project-link" to={`/projects/${model.project}`}>{model.projectName}</Link></p>
                                </div>
                                <div className="col-md-2 d-flex align-items-center">
                                    <p>{model.algorithm}</p>
                                </div>
                                <div className="col-md-2 d-flex align-items-center">
                                    <p>{model.modelType}</p>
                                </div>
                                <div className="col-md-1 d-flex align-items-center">
                                    {model.status == 'COMPLETE' && <p><span className="saved">Completed</span></p>}
                                    {model.status != 'COMPLETE' && model.status != 'FAIL' && <p><span className="blink">pending...</span></p>}
                                    {model.status == 'FAIL' && <p>
                                        <button value={model.exception} onClick={this.showError} className="btn btn-danger btn-failed">Failed</button>
                                        </p>}
                                </div>
                                <div className="col-md-1 d-flex align-items-center">
                                    {(model.algorithm === 'DecisionTreeClassifier' || 
                                        model.algorithm === 'RandomForestClassifier' ||
                                        model.algorithm === 'CatBoostClassifier') ?
                                        <Link to={`/models/${model._id}/tree`} className={model.status != 'COMPLETE' ? "btn btn-warning disabled" : "btn btn-warning"}>view</Link> :
                                        <button className="btn btn-warning" disabled={true}>view</button>
                                }
                                    
                                </div>
                                <div className="col-md-1 d-flex align-items-center">
                                    <button onClick={this.showModal} data-index={index} data-type="metrics" disabled={model.status != 'COMPLETE'} className="btn btn-primary">view</button>
                                </div>
                                <div className="col-md-1 d-flex align-items-center">
                                    <button onClick={this.showModal} data-index={index} data-type="log" disabled={model.status != 'COMPLETE'} className="btn btn-warning">view</button>
                                </div>
                                <div className="col-md-1 d-flex align-items-center">
                                    <Link to={`/models/${model._id}/predict`} className={model.status != 'COMPLETE' ? "btn btn-info disabled" : "btn btn-info"}>predict</Link>
                                </div>
                                <div className="col-md-1 d-flex align-items-center ">
                                    {(model.status === 'COMPLETE' || model.status === 'FAIL') &&  <button data-index={index} onClick={this.removeModel} className="btn btn-danger btn-rm">×</button>}
                                </div>

                            </div>
                        )
                    })}
                </div>
            </div>
                )
    }
}

export default ModelsDashboard