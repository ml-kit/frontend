import React from 'react'
import axios from 'axios'
import {Link} from 'react-router-dom'
import Bar from './charts/Bar'
import HeatMap from './charts/HeatMap'

const API_URL = process.env.REACT_APP_API_URL
const token = sessionStorage.getItem('token')

class SingleProject extends React.Component {
  constructor(props) {
    super(props)
    let id = props.match.params.id
    this.state = {
      id,
      target: "",
      isCategory: false,
      data: {
        columns: [],
        shape: []
      },
      saved: null,
      sent: null,
      onRender: false,
      chart: {
        type: "bar",
        target: "",
        presense: false,
        additional: [],
        data: [],
        error: null,
        binary_data: ""
      },
      model: {
        method: '',
        paramsList: [],
        activeParams: [],
        choosenParam: '',
        choosenParamType: '',
        choosenParamOptions: '',
        choosenValue: '',
        isHelp: false,
        scaling: false
      },
      allMethods: [],
      regressionMethods: [],
      classificationMethods: [],
    }
    this.categoryHandler = this.categoryHandler.bind(this)
    this.fillEmptyHandler = this.fillEmptyHandler.bind(this)
    this.mainColumnHandler = this.mainColumnHandler.bind(this)
    this.dropColumnHandler = this.dropColumnHandler.bind(this)
    this.saveOptions = this.saveOptions.bind(this)
    this.getAdditionalHeatmapValues = this.getAdditionalHeatmapValues.bind(this)
    this.getAdditionalBarValues = this.getAdditionalBarValues.bind(this)
    this.getChartValue = this.getChartValue.bind(this)
    this.getDataForChart = this.getDataForChart.bind(this)
    this.loadParams = this.loadParams.bind(this)
    this.addParam = this.addParam.bind(this)
    this.removeParam = this.removeParam.bind(this)
    this.chooseParamHandler = this.chooseParamHandler.bind(this)
    this.sendModel = this.sendModel.bind(this)
    this.getGroupedBarplots = this.getGroupedBarplots.bind(this)
    this.showModal = this.showModal.bind(this)
    this.scalingHandler = this.scalingHandler.bind(this)
  }

  componentDidMount() {
    this.loadProject(this.state.id)
  }

  async loadProject(id) {

    try {
      const res = await axios.get(`${API_URL}/projects/${id}`, {
        headers: {
          'Content-Type': 'multipart/form-data',
          "Authorization": "Bearer " + token
        }
      })

      const allMethods = await axios.get(`${API_URL}/models/meta`, {
        headers: {
          'Content-Type': 'multipart/form-data',
          "Authorization": "Bearer " + token
        }
      })

      let target = ""
      let isCategory = null

      res.data.columns.map((col) => {
        if (col.function == "target") {
          target = col.name
          isCategory = col.categories
        }
      })

      if (target == "") {
        res.data.columns[0].function = "target"
        target = res.data.columns[0].name
        isCategory = res.data.columns[0].categories
      }

      const {chart, model} = {
        ...this.state
      }

      const chartState = chart
      chartState.target = res.data.columns[0].name

      const methodsList = []

      Object.values(allMethods.data).map(t => t.map(m => methodsList.push(m)))

      const regressionMethods = allMethods.data.REGRESSION
      const classificationMethods = allMethods.data.CLASSIFICATION

      this.setState((prevState) => {
        return {
          ...prevState,
          chart: chartState,
          target,
          isCategory,
          model,
          allMethods: methodsList,
          regressionMethods,
          classificationMethods,
          data: res.data
        }
      })

    } catch (err) {
      
    }
  }

  categoryHandler(e) {
    const {data} = {
      ...this.state
    }
    const currentStateCategory = data
    const {name, value} = e.target
    let index = 0
    for (let i = 0; i < this.state.data.columns.length; i++) {
      let column = this.state.data.columns[i];
      if (column.name == name) {
        index = i;
      }
    }

    currentStateCategory.columns[index].categories = value == "true"

    let isCategory = this.state.isCategory

    if (this.state.target === name) {
      isCategory = value == "true"
    }

    this.setState((prevState) => {
      return {
        ...prevState,
        isCategory,
        data: currentStateCategory
      }
    });
  }

  getHelp() {
    const methodDescription = this.state.allMethods.filter((method) => method.name == this.state.model.method)

    
    if (methodDescription[0]) {
      const type = methodDescription[0].modelType
      const description = methodDescription[0].description
      return (
        <div>
          <h4>Model type:</h4>
          <span>{type}</span>
          <h4>Description</h4>
          <p>{description}</p>
        </div>)
    } else {
      return (
        <div>
          <h4>First, select method</h4>
        </div>
      )
    }
  }

  getParamsHelp() {
    const methodDescription = this.state.allMethods.filter((method) => method.name == this.state.model.method)

    
    if (methodDescription[0]) {
      const params = methodDescription[0].params
      return (
        <div className="row">
        <div className="col-12 mb-4">
        <h2 className="text-center ">Hyperparams help</h2>
        </div>
         
          {Object.keys(params).map((key) => {
            return (
              <div className="col-md-6 underline mb-4" key={key}>
                <h5>Parameter - <b>{key}</b></h5>
                <h6>Type - <b>{params[key].data_type}</b></h6>
                <h6>Default- <b>{params[key].default}</b></h6>
                <h5>Options:</h5>
                {params[key].options && params[key].options.map((option) => {
                  return <p key={option.value}><b>{option.value}</b> - {option.description}</p>
                })}
                <p>{params[key].description}</p>
              </div>
            )
          })}
        </div>)
    }
  }

  fillEmptyHandler(e) {
    const {data} = {
      ...this.state
    }
    const currentStateEmpty = data
    const {name, value} = e.target
    let index = 0
    for (let i = 0; i < this.state.data.columns.length; i++) {
      let column = this.state.data.columns[i];
      if (column.name == name) {
        index = i;
      }
    }

    currentStateEmpty.columns[index].fillNa = value;

    this.setState((prevState) => {
      return {
        ...prevState,
        data: currentStateEmpty
      }
    });
  }

  mainColumnHandler(e) {
    const {data} = {
      ...this.state
    }
    const currentStateMainColumn = data
    const {value} = e.target
    let index = 0
    let isCategory = ""
    let target = ""
    for (let i = 0; i < this.state.data.columns.length; i++) {
      let column = this.state.data.columns[i];
      if (column.name == value) {
        index = i;
        isCategory = currentStateMainColumn.columns[i].categories
        target = currentStateMainColumn.columns[i].name
      }
      if (column.function == "target") {
        currentStateMainColumn.columns[i].function = "feature"
      }
    }

    currentStateMainColumn.columns[index].function = "target";

    this.setState((prevState) => {
      return {
        ...prevState,
        isCategory,
        target,
        data: currentStateMainColumn
      }
    });
  }

  async saveOptions() {
    const options = []
    this.state.data.columns.map((column) => {
      let col = {
        name: column.name,
        fillNa: column.fillNa,
        categories: column.categories,
        function: column.function
      }
      options.push(col)
    })
    let saved = 'ok'

    try {
      const res = await axios.patch(`${API_URL}/projects/${this.state.id}/columns/`, options, {
        headers: {
          'Content-Type': 'application/json',
          "Authorization": "Bearer " + token
        }
      });
    } catch (err) {
      saved = 'error'
      if (err.response.status === 500) {
      } else {
      }
    }

    this.setState((prev) => {
      return {
        ...prev,
        saved
      }
    })

    setTimeout(function () {
      this.setState((prev) => {
        return {
          ...prev,
          saved: null
        }
      })
    }.bind(this), 2000);
  }

  dropColumnHandler(e) {
    const {data} = {
      ...this.state
    }
    const currentStateColumn = data
    const {value} = e.target
    let index = 0
    for (let i = 0; i < this.state.data.columns.length; i++) {
      let column = this.state.data.columns[i];
      if (column.name == value) {
        index = i;
      }
    }

    if (currentStateColumn.columns[index].function == "feature") {
      currentStateColumn.columns[index].function = "drop";
    } else if (currentStateColumn.columns[index].function == "drop") {
      currentStateColumn.columns[index].function = "feature";
    }

    this.setState((prevState) => {
      return {
        ...prevState,
        data: currentStateColumn
      }
    });
  }

  getChartValue(e) {
    const {chart} = {
      ...this.state
    }
    const currentStateChart = chart
    const {name, value} = e.target

    currentStateChart[name] = value
    if (name == 'type') {
      currentStateChart.additional = []
      currentStateChart.presense = false
    }
    if (name == 'target') {
      const removeIndex = currentStateChart.additional.indexOf(value)
      if (removeIndex != -1) {
        currentStateChart.additional.splice(removeIndex, 1)
      }
    }

    this.setState(prevState => {
      return {
        ...prevState,
        chart: currentStateChart
      }
    })
  }

  getAdditionalHeatmapValues(e) {
    const {chart} = {
      ...this.state
    }

    const currentStateHeatmap = chart
    const {options} = e.target
    const chartAdditional = []

    for (let i = 0; i < options.length; i++) {
      if (options[i].selected) {
        chartAdditional.push(options[i].value)
      }
    }

    currentStateHeatmap.additional = chartAdditional

    this.setState(prevState => {
      return {
        ...prevState,
        chart: currentStateHeatmap
      }
    })
  }

  getAdditionalBarValues(e) {
    const {chart} = {
      ...this.state
    }

    const currentStateBar = chart
    const value = e.target.value

    currentStateBar.additional = Array(value)

    this.setState(prevState => {
      return {
        ...prevState,
        chart: currentStateBar
      }
    })
  }

  getGroupedBarplots(e) {
    const {chart} = {
      ...this.state
    }
    const currentStateBarplots = chart

    const { name, value } = e.target

    if (name == "groupedBarplots1") {
      currentStateBarplots.additional[1] = value
    } else {
      currentStateBarplots.additional[0] = value
    }

    this.setState(prevState => {
      return {
        ...prevState,
        chart: currentStateBarplots
      }
    })
  }

  getChart() {
    try {
      switch (this.state.chart.type) {
        case "bar":
          return <Bar data={this.state.chart.data}></Bar>
        case "heatmap":
          return <HeatMap data={this.state.chart.data}></HeatMap>
        case "distribution":
          return <img src={`data:image/jpeg;base64,${this.state.chart.binary_data}`}/>
        case "Grouped Categories Plot":
          return <img src={`data:image/jpeg;base64,${this.state.chart.binary_data}`}/>
          break;

      }
    } catch (e) {
    }
  }

  async getDataForChart(e) {
    e.preventDefault()

    const {chart} = {
      ...this.state
    }
    const currentStateDataChart = chart

    currentStateDataChart.presense = (this.state.chart.target != '' && this.state.chart.additional.length > 0) || this.state.chart.type == 'distribution'

    if (currentStateDataChart.presense) {

      const chartSettings = {
        chartType: this.state.chart.type,
        mainColumn: this.state.chart.target
      }
      if (this.state.chart.type != 'distribution') {
        chartSettings.additionalColumns = this.state.chart.additional
      }

      try {
        this.setState((prevState) => {
          return {
            ...prevState,
            onRender: true
          }
        })

        if (this.state.chart.additional.includes(undefined)) {
          throw new Error('Some of the columns are empty!')
        }
        const id = this.state.id
        const res = await axios.post(`${API_URL}/projects/${id}/charts/`, chartSettings, {
          headers: {
            'Content-Type': 'application/json',
            "Authorization": "Bearer " + token,
            "responseType": 'blob'
          }
        }).then(result => {
          currentStateDataChart.error = null

          if (this.state.chart.type == "distribution" || this.state.chart.type == "Grouped Categories Plot") {
            currentStateDataChart.binary_data = result.data
          } else {
            currentStateDataChart.data = result.data
          }

        }).catch(err => {
          if (err.response) {
            if (err.response.status === 500) {
              currentStateDataChart.error = "Server was very tired to handle your request"
            } else if (err.response.status === 400) {
              currentStateDataChart.error = err.response.data.detail
            } else {
              currentStateDataChart.error = "There was a magic issue"
            }
          }
        })

      } catch (err) {
        currentStateDataChart.error = err.message
      }

    }
    else {
      currentStateDataChart.error = 'Some of the columns are empty!'
    }
    this.setState((prevState) => {
      return {
        ...prevState,
        onRender: false,
        chart: currentStateDataChart
      }
    })

  }

  loadParams(e) {

    const {model} = {
      ...this.state
    }
    const currentStateLoadParam = model

    currentStateLoadParam.activeParams = []

    const selectedMethod = e.target.value

    currentStateLoadParam.method = selectedMethod

    let params = [];

    if (selectedMethod != "none") {
      params = Object.keys(this.state.allMethods.filter((method) => method.name == selectedMethod )[0].params)
    }

    currentStateLoadParam.paramsList = params

    this.setState((prevState) => {
      return {
        ...prevState,
        model: currentStateLoadParam
      }
    })
  }

  chooseParamHandler(e) {
    const name = e.target.name
    const value = e.target.value

    const {model} = {
      ...this.state
    }
    const currentStateChooseParam = model
    currentStateChooseParam[name] = value

    if (name === 'choosenParam' ) {
      const method = this.state.allMethods.filter(method => method.name === this.state.model.method)[0]

  
      const data_type = method.params[value].data_type
  
      currentStateChooseParam.choosenParamType = data_type
  
      if (data_type === 'str') {
        const options = method.params[value].options
  
        currentStateChooseParam.choosenParamOptions = options
      } else {
        currentStateChooseParam.choosenParamOptions = []
      }
    }

    this.setState((prevState) => {
      return {
        ...prevState,
        model: currentStateChooseParam
      }
    })
  }

  addParam(e) {
    e.preventDefault()
    if (this.state.model.choosenParam != 'null' && this.state.model.choosenParam != '' && this.state.model.choosenValue != '') {

      const {model} = {
        ...this.state
      }
      const currentStateAddParam = model

      const method = this.state.allMethods.filter(m => m.name === this.state.model.method)[0]

      const type = method.params[this.state.model.choosenParam].data_type

      let value = currentStateAddParam.choosenValue

      switch (type) {
        case 'int':
          value = parseInt(value)
          if (isNaN(value)) {
            return
          }
          break
        case 'float':
          value = parseFloat(value)
          if (isNaN(value)) {
            return
          }
          break
      }

      currentStateAddParam.activeParams.push([currentStateAddParam.choosenParam, value])

      const removeIndex = currentStateAddParam.paramsList.indexOf(currentStateAddParam.choosenParam)

      if (removeIndex != -1) {
        currentStateAddParam.paramsList.splice(removeIndex, 1)
      }

      currentStateAddParam.choosenParam = ''
      currentStateAddParam.choosenParamType = ''
      currentStateAddParam.choosenValue = ''

      this.setState((prevState) => {
        return {
          ...prevState,
          chart: currentStateAddParam
        }
      })
    }

  }

  showModal(e) {
    e.preventDefault()

    const {model} = {
        ...this.state
    }

    const currentStateModal = model
    currentStateModal.isHelp = !currentStateModal.isHelp

    this.setState(prevState => {
        return {
            ...prevState,
            model: currentStateModal
        }
    })
}

  removeParam(e) {
    e.preventDefault()
    const name = e.target.dataset.name

    const {model} = {
      ...this.state
    }
    const currentStateRemoveParam = model

    currentStateRemoveParam.paramsList.push(name)

    let removeIndex = -1
    for (let i = 0; i < currentStateRemoveParam.activeParams.length; i++) {
      if (currentStateRemoveParam.activeParams[i][0] == name) {
        removeIndex = i
      }
    }

    if (removeIndex != -1) {
      currentStateRemoveParam.activeParams.splice(removeIndex, 1)
    }

    this.setState((prevState) => {
      return {
        ...prevState,
        chart: currentStateRemoveParam
      }
    })
  }

  scalingHandler() {
    const {model} = {
      ...this.state
    }
    const currentStateScaling = model

    currentStateScaling.scaling = !this.state.model.scaling

    this.setState((prevState) => {
      return {
        ...prevState,
        model: currentStateScaling
      }
    })
  }

  async sendModel() {

    if (this.state.model.method != "none" && this.state.model.method != "") {
      const settings = {
        projectId: this.state.id,
        modelMethod: this.state.model.method,
        scaling: this.state.model.scaling,
        hyper_params: Object.fromEntries(this.state.model.activeParams)
      }
  
      let sent = 'ok'
  
      const res = await axios.post(`${API_URL}/models/`, settings, {
        headers: {
          'Content-Type': 'application/json',
          "Authorization": "Bearer " + token,
        }
      }).catch(err => {
        sent = ''
        //console.log(err.response)
        // err.response.data.detail.map(t => {
        //   sent += t.msg
        // })
        sent = err.response.data.detail
      })
  
      this.setState((prev) => {
        return {
          ...prev,
          sent
        }
      })
  
      setTimeout(function () {
        this.setState((prev) => {
          return {
            ...prev,
            sent: null
          }
        })
      }.bind(this), 4000);
    }
  }

  render() {
    return (<div className="row">
      <div className="col-12">
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <Link to="/">Home</Link>
            </li>
            <li className="breadcrumb-item">
              <Link to="/">Projects</Link>
            </li>
            <li className="breadcrumb-item active" aria-current="page">{this.state.data.name}</li>
          </ol>
        </nav>
      </div>
      <div className="col-12">
        <div className="row mb-4 mt-4 main-field-row">
          <div className="col-md-5 col-6 pl-0">
            <h4>Main field, you want to predict</h4>
          </div>
          <div className="col-md-3 col-6">
            <select name="main-field" id="main-field" onChange={this.mainColumnHandler} defaultValue={this.state.target} className="flat-select">
              {this.state.target != '' && <option value={this.state.target}>{this.state.target}</option>}
              {
                this.state.data.columns.map(column => {
                  if (column.name != this.state.target) {
                    return <option value={column.name} key={column.name}>{column.name}</option>
                  }
                })
              }
            </select>
          </div>
          <div className="col-md-4 text-right">
            {this.state.saved && this.state.saved == 'ok' && <span className="saved">Saved!</span>}
            {this.state.saved && this.state.saved == 'error' && <span className="not-saved">Not saved!</span>}
            <button className="btn btn-success" onClick={this.saveOptions}>Save options</button>
          </div>
        </div>
      </div>
      <div className="col-1">
        <h5>Drop?</h5>
      </div>
      <div className="col-md-3 col-6">
        <h5>Criteria</h5>
      </div>
      <div className="col-md-2 col-6">
        <h5>Category or not</h5>
      </div>
      <div className="col-md-2 col-6 mb-md-0 mb-3">
        <h5>Instead of 0</h5>
      </div>
      <div className="col-md-2 col-6">
        <h5>Base stats</h5>
      </div>
      <div className="col-md-2 col-6 mb-md-0 mb-3">
        <h5>Unique(N)/Empty(N)</h5>
      </div>
      <div className="col-12">
        {
          this.state.data.columns.length > 0 && this.state.data.columns.map((item) => {
            return (<div className="row mb-3" key={item.name}>
              <div className="col-1">
                {item.function != "target" && <input type="checkbox" checked={item.function == "drop"} onChange={this.dropColumnHandler} value={item.name}/>}
              </div>
              <div className="col-md-3 col-6">
                {item.name}
                <span className="badge badge-info">{item.dataType}</span>
              </div>
              <div className="col-md-2 col-6">
                {
                  item.canBeCategories
                    ? <select className="flat-select" name={item.name} id={item.name} defaultValue={`${item.categories}`} onChange={this.categoryHandler}>
                        <option value="true">Category</option>
                        <option value="false">Not category</option>
                      </select>
                    : <p>can't be category</p>
                }
                </div>
                <div className="col-md-2 col-6">

                  <select className="flat-select" name={`${item.name}`} onChange={this.fillEmptyHandler} defaultValue={item.fillNa} id={`${item.name}-na`}>
                    <option value="drop">drop</option>
                    {
                      item.baseStats && Object.entries(item.baseStats).map((stat) => {
                        return <option key={`${stat[0]}`} value={stat[0]}>{stat[0]}</option>
                      })
                    }
                    {
                      item.categories && item.uniqueValues && item.uniqueValues.map((stat) => {
                        return <option key={stat} value={stat}>{stat}</option>
                      })
                    }
                  </select>
                </div>
                <div className="col-md-2 col-6 d-flex align-items-center flex-column base-stats">
                  {
                  item.baseStats && Object.entries(item.baseStats).map((stat) => {
                    return <p key={stat[0]}>{stat[0]}
                      - {stat[1]}</p>
                  })
                } {
                  !item.baseStats && <p>can't count</p>
                }
              </div>
              <div className="col-md-2 col-6 d-flex align-items-center">
                {item.nunique}/{item.na}
              </div>
            </div>)
          })
        }

      </div>
      <div className="col-12 mb-4">
        <form className="row mb-4">
          <div className="col-12 mt-4 mb-4">
            <h4 className="text-center main-field-row underline">Chart config</h4>
          </div>
          <div className="col-md-3">
            <label>
              Main field (X)
            </label>
            <select className="form-control" id="chart-target" defaultValue={this.state.traget} name="target" onChange={this.getChartValue}>
              {
                this.state.data.columns.map((col) => {
                  return <option key={col.name} value={col.name}>{col.name}</option>
                })
              }
            </select>
          </div>
          <div className="col-md-4">
            <label>
              Additional fields
            </label>
            {
              this.state.chart.type != "Grouped Categories Plot" && <select className="form-control" multiple={this.state.chart.type == 'heatmap'} disabled={this.state.chart.type == 'distribution'} id="bar-additional" name="additional" onChange={this.getAdditionalHeatmapValues}>
                  {
                    this.state.data.columns.map((col) => {
                      if (col.name != this.state.chart.target) {
                        return <option key={col.name} value={col.name}>{col.name}</option>
                      }

                    })
                  }
                </select>
            }

            {
              this.state.chart.type == 'Grouped Categories Plot' && <div className="row">
                  <label htmlFor="groupedBarplots0" className="col-sm-2 col-form-label">Y</label>
                  <div className="col-sm-10">
                    <select className="form-control" disabled={this.state.chart.type == 'distribution'} id="bar-additional" name="groupedBarplots0" onChange={this.getGroupedBarplots}>
                      {
                        this.state.data.columns.map((col) => {
                          if (col.name != this.state.chart.target && col.name != this.state.chart.additional[1]) {
                            return <option key={col.name} value={col.name}>{col.name}</option>
                          }

                        })
                      }
                    </select>
                  </div>
                  <label htmlFor="groupedBarplots1" className="col-sm-2 col-form-label mt-2">Hue</label>
                  <div className="col-sm-10">
                    <select className="form-control mt-2" disabled={this.state.chart.type == 'distribution'} id="bar-additional" name="groupedBarplots1" onChange={this.getGroupedBarplots}>
                      {
                        this.state.data.columns.map((col) => {
                          if (col.name != this.state.chart.target && col.name != this.state.chart.additional[0]) {
                            return <option key={col.name} value={col.name}>{col.name}</option>
                          }

                        })
                      }
                    </select>
                  </div>
                </div>
            }

          </div>
          <div className="col-md-3">
            <label>
              Chart type
            </label>
            <select className="form-control" id="chart-type" name="type" onChange={this.getChartValue}>
              <option value="bar">Bar</option>
              <option value="heatmap">HeatMap</option>
              <option value="distribution">Distribution</option>
              <option value="Grouped Categories Plot">Grouped Categories Plot</option>
            </select>
          </div>
          <div className="col-md-2 d-flex align-items-start">
            <button type="submit" disabled={this.state.onRender} className="btn btn-primary render-btn" onClick={this.getDataForChart}>Render</button>

          </div>
        </form>
      </div>
      <div className="col-md-12">
        {
          !this.state.chart.error && this.state.chart.presense && <div className="chart text-center">
              {this.getChart(this.state.chart.binary_data)}
            </div>
        }
        {
          this.state.chart.error && <div className="alert alert-danger text-center">
              {this.state.chart.error}
            </div>
        }
      </div>
      <div className="col-12">
        <div className="row mb-4 mt-4 underline">
        { this.state.model.isHelp && <div className="metrics-modal">
        <div className="row">
            <div className="col-md-6 offset-md-3 methodHelp">
            <h2 className="text-center">Help</h2>
            <a href="#" onClick={this.showModal} className="btn btn-danger close-btn">close</a>
            {this.getHelp()}
              </div>
            </div> 
          </div>
          }
          <div className="col-md-12 col-6 ">
            <h4 className="text-center underline">Model config</h4>
          </div>
          <div className="col-md-1">
            <label>Scaling</label>
            <input type="checkbox" name="scaling" onChange={this.scalingHandler} />
          </div>
          <div className="col-md-3">
            <label>Select one of the methods</label>
            <select name="method" className="form-control" defaultValue="none" onChange={this.loadParams}>
            <option value="none">none</option>
            {
              this.state.data.columns.length > 0 && this.state.isCategory === false &&

              this.state.regressionMethods && this.state.regressionMethods.map((method) => {
                return <option key={method.name} value={method.name}>{method.display}</option>
              })
            } 

            {
              this.state.data.columns.length > 0 && this.state.isCategory === true &&

              this.state.classificationMethods.map((method) => {
                return <option key={method.name} value={method.name}>{method.display}</option>
              })
            } 

            </select>
          </div>
          <div className="col-md-1">
            <button onClick={this.showModal} className="btn btn-primary mt-4 addParam btn-help">?</button>
          </div>
          <div className="col-md-4">
            <label>hyperparams</label>
            <div className="form-group row">
              <div className="col-8">
                <select name="choosenParam" defaultValue="null" onChange={this.chooseParamHandler} className="form-control">
                  <option value="null">Choose param</option>
                  {
                    this.state.model.paramsList.map((param) => {
                      return <option key={param} value={param}>{param}</option>
                    })
                  }
                </select>
              </div>
              <div className="col-4">
                {this.state.model.choosenParamType === 'str' && 
                  <select name="choosenValue" className="form-control" defaultValue="null" onChange={this.chooseParamHandler}>
                    <option value="null">Choose</option>
                    {this.state.model.choosenParamOptions.map(option => {
                      return <option key={option.value} value={option.value}>{option.value}</option>
                    })}
                  </select>
                }

                {this.state.model.choosenParamType === 'bool' && 
                  <select name="choosenValue" className="form-control" defaultValue="null" onChange={this.chooseParamHandler}>
                    <option value="null">Choose</option>
                    <option value={true} >True</option>
                    <option value={false} >False</option>
                  </select>
                }

                {(this.state.model.choosenParamType === 'int' || this.state.model.choosenParamType === 'float') &&
                <input className="form-control" onChange={this.chooseParamHandler} placeholder="value" type="number" name="choosenValue"/>}

                {this.state.model.choosenParamType === 'array' &&
                <input className="form-control" onChange={this.chooseParamHandler} placeholder="value" type="text" name="choosenValue"/>}
              </div>
            </div>
          </div>
          <div className="col-md-1 pt-2">
            <a href="#" onClick={this.addParam} className="btn btn-primary mt-4 addParam">+</a>
          </div>
          <div className="col-md-2 pt-2">
            <button className="btn btn-success mt-4" onClick={this.sendModel}>Send to learning</button>
              {this.state.sent && this.state.sent === 'ok' && <span className="saved sentMsg">Successfully sent to learning!</span>}
              {this.state.sent && this.state.sent !== 'ok' && <span className="not-saved sentMsg">{this.state.sent}</span>}
          </div>
          <div className="col-md-12 hyperParams mb-3">
            <p>hyperparams:</p>
            {
              this.state.model.activeParams.map((param) => {
                return <div key={param[0]} className="single-param">{param[0]}
                  - {param[1]}<a href="#" data-name={param[0]} className="remove-param" onClick={this.removeParam}>X</a>
                </div>
              })
            }
          </div>
          {this.getParamsHelp()}
        </div>
      </div>

    </div>)
  }
}

export default SingleProject
