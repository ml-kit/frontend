import React, {Fragment, useState} from 'react';
import {Redirect} from 'react-router-dom'
import Message from './Message';
import Progress from './Progress';
import axios from 'axios';

const API_URL = process.env.REACT_APP_API_URL
const token = sessionStorage.getItem('token')

const FileUpload = props => {
    const [file,
        setFile] = useState('');
    const [name,
        setName] = useState('');
    const [filename,
        setFilename] = useState('Choose File');
    const [message,
        setMessage] = useState('');
    const [uploadPercentage,
        setUploadPercentage] = useState(0);
    const [toProj,
        setProject] = useState("");

    const onChangeFile = e => {
        setFile(e.target.files[0]);
        setFilename(e.target.files[0].name);
    };

    const onChangeText = e => {
        setName(e.target.value)
    }

    const linkToProject = id => {
        setTimeout(() => {
            setProject(id)
        }, 3000)
    }

    const onSubmit = async e => {
        e.preventDefault();
        const formData = new FormData();
        formData.append('file', file);
        formData.append('name', name);

        try {
            const res = await axios.post(`${API_URL}/projects/`, formData, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    "Authorization": "Bearer " + token
                },
                onUploadProgress: progressEvent => {
                    setUploadPercentage(parseInt(Math.round((progressEvent.loaded * 100) / progressEvent.total)));

                    setTimeout(() => setUploadPercentage(0), 10000);
                }
            });
            setMessage('File Uploaded, You will be redirect in 3 sec.');
            linkToProject(res.data._id)
        } catch (err) {
            if (err.response.status === 500) {
                setMessage('There was a problem with the server');
            } else {
                setMessage(err.response.data.msg);
            }
        }
    };

    return (
        <div className="row">
            <Fragment>
                <div className="col-md-6 offset-md-3 offset-0 text-center">
                    <h2 className="text-center mt-3 mb-2">New project</h2>
                    {message
                        ? <Message msg={message}/>
                        : null}
                </div>
                <form onSubmit={onSubmit} className="col-md-6 offset-md-3 offset-0 mt-3">
                    <div>
                        <input
                            type='text'
                            className='custom-text-input form-control mb-2'
                            id='customText'
                            required
                            placeholder='Project name'
                            onChange={onChangeText}/>
                    </div>
                    <div className='custom-file mb-4'>
                        <input
                            type='file'
                            required
                            className='custom-file-input'
                            id='customFile'
                            onChange={onChangeFile}/>

                        <label className='custom-file-label' htmlFor='customFile'>
                            {filename}
                        </label>
                    </div>

                    <Progress percentage={uploadPercentage}/>

                    <input type='submit' value='Upload' className='btn btn-primary btn-block mt-4'/>
                </form>
            </Fragment>
            {toProj.length > 0
                ? <Redirect
                        to={{
                        pathname: `/projects/${toProj}`
                    }}></Redirect>
                : null}
        </div>
    );
};

export default FileUpload;