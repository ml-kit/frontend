import React from 'react'
import {Row} from 'react-bootstrap'
import {connect} from 'react-redux'
import $ from 'jquery'
import {Link} from 'react-router-dom'
import FileUpload from './FileUpload'
import axios from 'axios'
import Message from './Message'

const API_URL = process.env.REACT_APP_API_URL
const token = sessionStorage.getItem('token')

class Dashboard extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            projects: [],
            message: ''
        }
        this.deleteProject = this
            .deleteProject
            .bind(this)
    }

    async deleteProject(e) {
        e.preventDefault()
        const id = e.target.dataset.id
        try {
            await axios.delete(`${API_URL}/projects/${id}/`, {
                headers: {
                    "Authorization": "Bearer " + token
                }
            });

            await this.getProjects()
        } catch (err) {
        }
    }

    async getProjects() {

        try {
            const res = await axios.get(`${API_URL}/projects/`, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    "Authorization": "Bearer " + token
                }
            });
            this.setState({projects: res.data, message: ''})
        } catch (err) {
            let msg = ''
            if (err === undefined) {
                msg = 'Some problem'
            } else if (err.response.status === 500) {
                msg = 'There was a problem with the server'
            } else if (err.response.status === 404) {
                msg = '404'
            } else {
                msg = 'Some problem'
            }
            msg = 'Some problem'
            this.setState((prevState) => {
                return {
                    ...prevState,
                    message: msg
                }
            })
        }
    }

    async componentDidMount() {
        this.getProjects()
    }

    render() {
        return (
            <Row>
                <div className="col-12">
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item active" aria-current="page">Home</li>
                        </ol>
                    </nav>
                </div>

                <div className="col-12">
                    <h1 className="text-center">Projects dashboard</h1>
                    <FileUpload></FileUpload>
                    <h2 className="text-center mt-4 mb-2">All projects</h2>
                    {this.state.message.length > 0
                        ? <Message msg={this.state.message}/>
                        : null}
                    <div className="row all-projects mt-4">
                        {this
                            .state
                            .projects
                            .map((proj) => {
                                return (
                                    <div className="col-md-3 mb-4 mt-2 text-center" key={proj._id}>
                                        <Link className="project" to={`/projects/${proj._id}`}>{proj.name}</Link>
                                        <Link
                                            className="delete"
                                            onClick={this.deleteProject}
                                            data-id={proj._id}
                                            to={`/projects/${proj._id}`}>delete</Link>
                                    </div>
                                )
                            })}
                    </div>
                </div>
            </Row>
        )
    }
}

const mapStateToProps = state => {
    return {loggedIn: state.auth.loggedIn, user: state.auth.user}
}

export default connect(mapStateToProps)(Dashboard)