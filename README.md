# CSV to ML

### Installation

Dillinger requires [Node.js](https://nodejs.org/) v4+ to run.

Install the dependencies and devDependencies and start the server.

```sh
$ cd frontend
$ npm install
$ npm start
```

License
----

MIT